//
//  ViewController.swift
//  AppleMusicRemoteDemo
//
//  Created by Serhii Kobzin on 23.05.2021.
//

import MediaPlayer
import UIKit

class ViewController: UIViewController {
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var prevButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppleMusicRemoteService.shared.subscribeOnPlayerStateChanges(callbackHandler: { [weak self] state in
            guard let `self` = self else { return }
            switch state {
            case .disconnected:
                self.titleLabel.text = ""
                self.nextButton.isHidden = true
                self.playButton.setTitle("Connect", for: .normal)
                self.prevButton.isHidden = true
            case .paused(let title):
                self.titleLabel.text = title
                self.nextButton.isHidden = false
                self.playButton.setTitle("Play", for: .normal)
                self.prevButton.isHidden = false
            case .playing(let title):
                self.titleLabel.text = title
                self.nextButton.isHidden = false
                self.playButton.setTitle("Pause", for: .normal)
                self.prevButton.isHidden = false
            }
        })
    }
    
    @IBAction func playButtonTouchUpInside(_ sender: UIButton) {
        AppleMusicRemoteService.shared.play(failureHandler: {
            
        })
    }
    
    @IBAction func nextButtonToushUpInside(_ sender: UIButton) {
        AppleMusicRemoteService.shared.playNext()
    }
    
    @IBAction func prevButtonTouchUpInside(_ sender: UIButton) {
        AppleMusicRemoteService.shared.playPrevious()
    }
}
