//
//  AppleMusicRemoteService.swift
//  SpotifyRemoteDemo
//
//  Created by Serhii Kobzin on 22.05.2021.
//

import Foundation
import MediaPlayer

enum PlayerState {
    case disconnected
    case paused(title: String)
    case playing(title: String)
}

class AppleMusicRemoteService: NSObject {
    static let shared = AppleMusicRemoteService()
    
    private let player = MPMusicPlayerController.systemMusicPlayer
    private var playerStateChangesCallbackHandler: ((PlayerState) -> Void)?
    
    private var playerState: PlayerState = .disconnected {
        didSet {
            playerStateChangesCallbackHandler?(playerState)
        }
    }
    
    func connect() {
        registerForNotifications()
        playerStateDidChange()
    }
    
    func disconnect() {
        unregisterFromNotifications()
    }
    
    func play(failureHandler: @escaping () -> Void) {
        switch playerState {
        case .disconnected, .paused:
            player.play()
        case .playing:
            player.pause()
        }
    }
    
    func playNext() {
        player.skipToNextItem()
    }
    
    func playPrevious() {
        player.skipToPreviousItem()
    }
    
    func subscribeOnPlayerStateChanges(callbackHandler: @escaping (PlayerState) -> Void) {
        playerStateChangesCallbackHandler = callbackHandler
    }
}

// MARK: - Notifications Observers
extension AppleMusicRemoteService {
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(playerStateDidChange), name: NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerStateDidChange), name: NSNotification.Name.MPMusicPlayerControllerPlaybackStateDidChange, object: nil)
    }
    
    private func unregisterFromNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.MPMusicPlayerControllerPlaybackStateDidChange, object: nil)
    }
    
    @objc private func playerStateDidChange() {
        player.repeatMode = player.repeatMode // Kostyl for player state updating
        switch player.playbackState {
        case .stopped:
            playerState = .disconnected
        case .paused:
            guard let nowPlayingItem = player.nowPlayingItem else { return }
            let title = "\(nowPlayingItem.artist ?? "") - \(nowPlayingItem.title ?? "")"
            playerState = .paused(title: title)
        case .playing:
            guard let nowPlayingItem = player.nowPlayingItem else { return }
            let title = "\(nowPlayingItem.artist ?? "") - \(nowPlayingItem.title ?? "")"
            playerState = .playing(title: title)
        default:
            break
        }
    }
}
